import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { Observable } from 'rxjs';
import { ApiService } from './api.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  public data: any;
  constructor(
    private apiService: ApiService,
  ) {}
  ngOnInit(): void {
    this.apiService.getData("http://192.168.48.2:8080").subscribe(
      (res) => {
        console.log(res);
      }
    );
  }
}
