﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Reflection.Metadata;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Service.Model;

namespace VTVcab.DAO.VTVcabSell
{
    public partial class VTVcabContext : DbContext
    {
        public VTVcabContext()
        {
        }
        public VTVcabContext(DbContextOptions<VTVcabContext> options)
           : base(options)
        {
        }      
        public virtual DbSet<users> users { get; set; }
        

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {

            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            OnModelCreatingPartial(modelBuilder);
        }
        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
