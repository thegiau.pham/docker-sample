﻿using Microsoft.Extensions.Configuration;

namespace Utility
{
    public class ConfigUtils
    {
        public static string GetConnectionString(string key)
        {
            string projectPath = AppDomain.CurrentDomain.BaseDirectory.Split(new string[] { @"bin\" }, StringSplitOptions.None)[0];
            IConfigurationRoot configuration = new ConfigurationBuilder()
                   .SetBasePath(projectPath)
                   .AddJsonFile("appsettings.json")
                   .Build();
            return configuration.GetConnectionString(key);
        }
        public static string GetKeyValue(string section, string key)
        {
            try
            {
                string projectPath = AppDomain.CurrentDomain.BaseDirectory.Split(new string[] { @"bin\" }, StringSplitOptions.None)[0];
                IConfigurationRoot configuration = new ConfigurationBuilder()
                       .SetBasePath(projectPath)
                       .AddJsonFile("appsettings.json")
                       .Build();
                return configuration.GetSection(section)[key];
            }
            catch { }
            return "";
        }

    }
}