﻿using Microsoft.EntityFrameworkCore;
using Service.Model;
using VTVcab.DAO.VTVcabSell;

namespace APITeamplate.Services
{
    public class SellService : ISellService
    {
        private readonly VTVcabContext _dbContext;
        public SellService(VTVcabContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<ResponseUser> LoginPage()
        {
            var infoUser = new ResponseUser();
            try
            {
                infoUser.code = 0;
                var user = await _dbContext.users.Where(l => l.deleted == "0").FirstOrDefaultAsync();
                infoUser.data = user;
                infoUser.code = 1;
                infoUser.message = "Success";
            }
            catch(Exception exx)
            {

            }
            return infoUser;
        }
    }
}
