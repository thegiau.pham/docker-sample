﻿using APITeamplate.Services;
using Microsoft.AspNetCore.Mvc;

namespace APITeamplate.Controllers
{
    [Route("api/Home")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly ISellService _sellService;
        public HomeController(ISellService sellService)
        {
            _sellService = sellService;
        }
        [HttpGet]
        [Route("LoginPage")]
        public async Task<object> LoginPage()
        {
            return await _sellService.LoginPage();
        }
    }
}
