﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Model
{
    public class users
    {
        public int id { get; set; }
        public string userName { get; set; }
        public string pass { get; set; }
        public string fullName { get; set; }
        public string descriptions { get; set; }
        public string email { get; set; }
        public string imageUrl { get; set; }
        public DateTime createdDate { get; set; }
        public DateTime updatedDate { get; set; }
        public int createdBy { get; set; }
        public int updatedBy { get; set; }
        public string deleted { get; set; }
    }
    internal class Response
    {
    }
    public class ResponseUser
    {
        public int code { get; set; }
        public string message { get; set; }
        public users data { get; set; }
        public List<string> roles { get; set; }
    }
    public class InputLogin
    {
        public string userName { get; set; }
        public string pass { get; set; }
        public string key { get; set; }

    }
}
